# java ee rest application

This project was made to learn the basics of java ee by building a rest-api for a small todo-app and testing it

## Requirements

java 11
maven
docker

## Build and run

mvn clean package

docker-compose up -d --build