package com.jchaaban.javaeerestapp.service.persistence.todo;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

import java.nio.file.Paths;

import static io.restassured.RestAssured.given;
@Testcontainers
public class RestApplicationIT {

    private static final MountableFile warFile = MountableFile.forHostPath(
            Paths.get("target/rest-app.war").toAbsolutePath(), 511);
    @Container
    private static final GenericContainer microContainer =
            new GenericContainer(DockerImageName.parse("payara/micro:5.2022.2-jdk11"))
                    .withExposedPorts(8080)
                    .withCopyFileToContainer(warFile, "/opt/payara/deployments/app.war")
                    .waitingFor(Wait.forLogMessage(".* Payara Micro .* ready in .*\\s", 1))
                    .withCommand("--deploy /opt/payara/deployments/app.war --noCluster --contextRoot /");

    @Test
    public void checkContainerIsRunning(){
        String host = microContainer.getHost();
        Integer port = microContainer.getMappedPort(8080);

        // Construct the correct URL using the host and dynamic port
        String url = "http://" + host + ":" + port + "/api/greet/hello";

        // Perform the test
        given()
                .log().all() // Log the request details
                .contentType(ContentType.JSON)
                .when()
                .get(url)
                .then()
                .log().ifValidationFails()
                .log().body()
                .assertThat()
                .statusCode(200);
    }
}
