package com.jchaaban.javaeerestapp.service.persistence.todo;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.MountableFile;

import java.nio.file.Paths;
import java.util.function.Consumer;


@Testcontainers
public class DbApplicationIT {

    static Network network = Network.newNetwork();
    @Container
    private static final PostgreSQLContainer<?> postgresContainer = new PostgreSQLContainer<>("postgres:latest")
            .withNetwork(network)
            .withDatabaseName("postgres")
            .withUsername("postgres")
            .withPassword("admin")
            .withNetworkAliases("postgres");

    private static final MountableFile warFile = MountableFile.forHostPath(
            Paths.get("target/restapp.war").toAbsolutePath(), 0777);
    @Container
    private static final GenericContainer microContainer =
            new GenericContainer("payara/micro:5.2021.9-jdk11")
                    .withExposedPorts(8080)
                    .withCopyFileToContainer(warFile, "/opt/payara/deployments/app.war")
                    .withCommand("--deploy /opt/payara/deployments/app.war --contextRoot /")
                    .withLogConsumer((Consumer<OutputFrame>) outputFrame -> System.out.println(outputFrame.getUtf8String()));

}
