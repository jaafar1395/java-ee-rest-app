package com.jchaaban.javaeerestapp.service.query.todo;


import com.jchaaban.javaeerestapp.entity.TodoUser;
import com.jchaaban.javaeerestapp.session.UserSession;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class UserQueryService {

    @PersistenceContext(unitName = "rest-app") // when we have one persistence unit we don't have to
    private EntityManager entityManager;

    @Inject
    private UserSession userSession;
    public List<TodoUser> fetchAllTodoUsers(){
        return entityManager
                .createNamedQuery(TodoUser.FETCH_ALL_USERS, TodoUser.class)
                .getResultList();
    }

    public TodoUser fetchTodoUserByEmail(String email){
        return entityManager
                .createNamedQuery(TodoUser.FETCH_USER_BY_EMAIL, TodoUser.class)
                .setParameter("email", email)
                .getSingleResult();
    }


    public TodoUser fetchUserByIdAndEmail(String id){
        return entityManager
                .createNamedQuery(TodoUser.FETCH_USER_BY_ID_AND_EMAIL, TodoUser.class)
                .setParameter("id", id).setParameter("email", userSession.getEmail())
                .getSingleResult();
    }

    public List<TodoUser> fetchTodoUsersByName(String name){
        return entityManager
                .createNamedQuery(TodoUser.FETCH_USER_BY_FULL_NAME, TodoUser.class)
                .setParameter("name", "%" + name + "%")
                .getResultList();
    }
}
