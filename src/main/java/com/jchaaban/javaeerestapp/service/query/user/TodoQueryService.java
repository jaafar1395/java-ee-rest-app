package com.jchaaban.javaeerestapp.service.query.user;


import com.jchaaban.javaeerestapp.entity.Todo;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import javax.ejb.Stateless;

import java.util.List;

@Stateless
public class TodoQueryService {

    @PersistenceContext(unitName = "rest-app") // when we have one persistence unit we don't have to
    private EntityManager entityManager;

    public List<Todo> fetchAllTodos(){
        return entityManager
                .createNamedQuery(Todo.FETCH_ALL_TODOS, Todo.class)
                .getResultList();
    }

    public Todo fetchTodoByEmail(String email){
        return entityManager
                .createNamedQuery(Todo.FETCH_TODO_BY_USER_EMAIL, Todo.class)
                .setParameter("email", email)
                .getSingleResult();
    }
}
