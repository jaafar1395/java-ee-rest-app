package com.jchaaban.javaeerestapp.service.persistence.todo;



import com.jchaaban.javaeerestapp.entity.Todo;
import com.jchaaban.javaeerestapp.entity.TodoUser;
import com.jchaaban.javaeerestapp.service.query.todo.UserQueryService;
import com.jchaaban.javaeerestapp.session.UserSession;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import javax.inject.Inject;


@Stateless  // used for lightweight, stateless operations, such as service layers, where each request doesn't rely on
            // the previous request's state
public class TodoPersistenceService {

    // Using this annotation we let the app server establish a relationship between the injected EntityManager and the
    // specified persistence unit defined in persistence.xml so this entity manager will use the config specified in the
    // persistence unit.
    @PersistenceContext(unitName = "rest-app") // when we have one persistence unit we don't have to
    private EntityManager entityManager;
    @Inject
    private UserSession userSession;
    @Inject
    private UserQueryService userQueryService;

    public Todo saveTodo(Todo todo){

        String email = userSession.getEmail();
        TodoUser todoUser = userQueryService.fetchUserByIdAndEmail(email);

        if (todo.getId() == null) {
            todo.setTodoUser(todoUser);
            entityManager.persist(todo);
        } else {
            entityManager.merge(todo);
        }
        return todo;
    }
}
