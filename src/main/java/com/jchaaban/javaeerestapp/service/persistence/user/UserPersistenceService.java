package com.jchaaban.javaeerestapp.service.persistence.user;

import com.jchaaban.javaeerestapp.entity.TodoUser;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Stateless
public class UserPersistenceService {

    @PersistenceContext(unitName = "rest-app") // when we have one persistence unit we don't have to
    private EntityManager entityManager;


    public TodoUser saveTodoUser(TodoUser user){

        if (user.getId() == null){
            entityManager.persist(user);
        } else {
            entityManager.merge(user);
        }
        return user;
    }

}
