package com.jchaaban.javaeerestapp;


import com.jchaaban.javaeerestapp.entity.TodoUser;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

@Path("/greet")
public class SpeakerApplication extends Application {


	// just to invoke the entity manager so that tha db table get created
	public SpeakerApplication() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("rest-app");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();

		em.persist(new TodoUser("test user", "test.user@gmail.com", "test"));
		em.flush();
		em.getTransaction().commit();
		em.close();
		emf.close();
	}



	@GET
	@Path("/hello")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBrands() {
		return "Hello";
	}

}
