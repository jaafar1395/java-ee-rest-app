package com.jchaaban.javaeerestapp.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@Entity
@NamedQuery(name = Todo.FETCH_ALL_TODOS, query = "SELECT todo FROM Todo todo order by todo.creationDate")
public class Todo {

    public static final String FETCH_TODO_BY_USER_EMAIL = "FETCH_TODO_BY_USER_EMAIL";
    public static final String FETCH_ALL_TODOS = "FETCH_ALL_TODOS";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "A todo task must be set")
    @Size(min = 3, max = 140, message = "The minimum characters length should be 3 and max 140")
    private String task;

    private LocalDate creationDate;

    @NotNull(message = "Due date must be set")
    @FutureOrPresent // this annotation will ensure the date will be in the present or future
    @JsonbDateFormat("yyyy-MM-dd") // ads validation constrains on the format of this LocalDate object
    private LocalDate doDate;

    private boolean completed;

    private boolean archived;

    private boolean remind;

    @ManyToOne
    @JoinColumn(name = "todo_user_id")
    private TodoUser todoUser;
}
