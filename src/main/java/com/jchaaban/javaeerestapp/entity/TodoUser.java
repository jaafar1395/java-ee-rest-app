package com.jchaaban.javaeerestapp.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

// we can have as much named query as we want, the name has to be unique these are compiled with the class
@Data
@Entity
@Table(name = "todo_user")
@NamedQuery(name = TodoUser.FETCH_USER_BY_EMAIL, query = "SELECT todoUser FROM TodoUser todoUser WHERE todoUser.email = :email")
@NamedQuery(name = TodoUser.FETCH_USER_BY_ID_AND_EMAIL, query = "SELECT todoUser FROM TodoUser todoUser WHERE todoUser.id = :id AND todoUser.email = :email")
@NamedQuery(name = TodoUser.FETCH_ALL_USERS, query = "SELECT todoUser FROM TodoUser todoUser order by todoUser.fullname")
@NamedQuery(name = TodoUser.FETCH_USER_BY_FULL_NAME, query = "SELECT todoUser FROM TodoUser todoUser WHERE todoUser.fullname LIKE :firstname")
public class TodoUser {

    public static final String FETCH_USER_BY_ID_AND_EMAIL = "FETCH_USER_BY_ID";

    private static final String PASSWORD_REGEX = "^[a-zA-Z0-9!@#$%^&*()-_=+\\[\\]{}|;:'\",.<>?/\\\\]*$";
    public static final String FETCH_USER_BY_EMAIL = "FETCH_USER_BY_EMAIL";
    public static final String FETCH_USER_BY_FULL_NAME = "FETCH_USER_BY_NAME";
    public static final String FETCH_ALL_USERS = "FETCH_ALL_USERS";

    public TodoUser(String fullname, String email, String password) {
        this.fullname = fullname;
        this.email = email;
        this.password = password;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "A name must be set")
    @Size(min = 3, max = 50, message = "The minimum characters length should be 3 and max 50")
    private String fullname;

    @Column(length = 100)
    @NotEmpty(message = "A todo task must be set")
    @Email(message = "The email has to have the following format: user@domain.com")
    private String email;

    @NotNull(message = "Password cannot be empty")
    @Size(min = 3, max = 100, message = "The minimum characters length should be 3 and max 100")
    @Pattern(regexp = PASSWORD_REGEX, message = "Password must contain only letters, digits, and special characters: !@#$%^&*()-_=+[]{}|;:'\",.<>?/\\")
    private String password;

}
