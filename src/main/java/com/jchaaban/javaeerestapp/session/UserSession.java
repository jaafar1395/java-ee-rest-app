package com.jchaaban.javaeerestapp.session;

import lombok.Data;

import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

@Data
@SessionScoped
public class UserSession implements Serializable {

    private String email;



}
