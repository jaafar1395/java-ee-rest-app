FROM tomcat:9-jdk11-openjdk

RUN rm -rf /usr/local/tomcat/webapps/ROOT

COPY target/restapp.war /usr/local/tomcat/webapps/ROOT.war

EXPOSE 8080

CMD ["catalina.sh", "run"]

#  for some reason the app is getting deployed but it is not reachable

#FROM quay.io/wildfly/wildfly:latest-jdk11
#
#ADD target/restapp.war /opt/jboss/wildfly/standalone/deployments/
#
#EXPOSE 8080 9990 8443
#
#CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]

